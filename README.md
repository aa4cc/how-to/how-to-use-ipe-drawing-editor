# How to use IPE Drawing Editor

This repository contains several IPE drawings that might come handy.
The drawings are saved directly in .pdf format. Use "File -> Open" to open and edit the files.